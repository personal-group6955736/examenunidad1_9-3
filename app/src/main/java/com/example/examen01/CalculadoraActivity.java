package com.example.examen01;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        calculadora = new Calculadora();

        EditText txtNum1 = findViewById(R.id.txtNum1);
        EditText txtNum2 = findViewById(R.id.txtNum2);
        TextView lblResultado = findViewById(R.id.lblResultado);
        TextView lblNombre = findViewById(R.id.lblUsuario);

        Button btnSuma = findViewById(R.id.btnSuma);
        Button btnResta = findViewById(R.id.btnResta);
        Button btnMult = findViewById(R.id.btnMult);
        Button btnDiv = findViewById(R.id.btnDiv);
        Button btnRegresar = findViewById(R.id.btnRegresar);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);

        lblNombre.setText("Usuario: José Lopez");

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setValuesFromInput(txtNum1, txtNum2)) {
                    lblResultado.setText("Resultado: " + calculadora.suma());
                }
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setValuesFromInput(txtNum1, txtNum2)) {
                    lblResultado.setText("Resultado: " + calculadora.resta());
                }
            }
        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setValuesFromInput(txtNum1, txtNum2)) {
                    lblResultado.setText("Resultado: " + calculadora.multiplicacion());
                }
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setValuesFromInput(txtNum1, txtNum2)) {
                    try {
                        lblResultado.setText("Resultado: " + calculadora.division());
                    } catch (ArithmeticException e) {
                        Toast.makeText(CalculadoraActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNum2.setText("");
                txtNum1.setText("");
                lblResultado.setText("Resultado: ");
            }
        });
    }

    private boolean setValuesFromInput(EditText txtNum1, EditText txtNum2) {
        try {
            float num1 = Float.parseFloat(txtNum1.getText().toString());
            float num2 = Float.parseFloat(txtNum2.getText().toString());

            calculadora.setNum1(num1);
            calculadora.setNum2(num2);

            return true;
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor, ingrese números válidos.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
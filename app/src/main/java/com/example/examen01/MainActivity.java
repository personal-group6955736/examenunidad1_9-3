package com.example.examen01;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText txtUsuario = findViewById(R.id.txtUsuario);
        EditText txtContraseña = findViewById(R.id.txtContraseña);
        Button btnIngresar = findViewById(R.id.btnIngresar);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = getResources().getString(R.string.user);
                String pass = getResources().getString(R.string.pass);
                if(txtUsuario.getText().toString().matches("") ||
                        (txtContraseña.getText().toString().matches(""))){
                    Toast.makeText(MainActivity.this, "Faltó capturar el usuario", Toast.LENGTH_SHORT).show();
                }
                else if (txtUsuario.getText().toString().equals(user) &&
                        txtContraseña.getText().toString().equals(pass)) {
                    Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}